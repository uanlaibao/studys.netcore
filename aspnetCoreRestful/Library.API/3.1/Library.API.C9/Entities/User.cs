﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Library.API.C9.Entities
{
    public class User : IdentityUser
    {
        public DateTimeOffset BirthDate { get; set; }
    }

    public class Role : IdentityRole
    {
    }
}