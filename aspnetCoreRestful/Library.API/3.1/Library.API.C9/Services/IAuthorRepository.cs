﻿using Library.API.C9.Entities;
using Library.API.C9.Helpers;
using System;
using System.Threading.Tasks;

namespace Library.API.C9.Services
{
    public interface IAuthorRepository : IRepositoryBase<Author>, IRepositoryBase2<Author, Guid>
    {
        Task<PagedList<Author>> GetAllAsync(AuthorResourceParameters parameters);
    }
}