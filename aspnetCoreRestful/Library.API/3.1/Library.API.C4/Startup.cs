﻿using Library.API.C4.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Library.API.C4
{
    /// <summary>
    /// 用来配置服务和请求管道
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 初始化配置文件
        /// </summary>
        /// <param name="configuration">读取网站配置，配置文件：appsettings.json</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        /// <summary>
        /// 基础配置
        /// </summary>
        /// <param name="app">请求执行管道</param>
        /// <param name="env">环境</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            ///检查是否开发环境
            if (env.IsDevelopment())
            {
                //  异常时使用调试页面
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //  在处理请求管道中使用“安全认证HSTS”
                app.UseHsts();
                ////    在处理请求管道中使用“异常”  
                //  app.UseExceptionHandler("/Home/Error");
            }

            ////  在处理请求管道中使用“静态文件”  
            //  app.UseStaticFiles();

            //  在处理请求管道中使用“https重定向”(检查当前项目启动后，监听的是否是多个端口，其中如果有协议是Https—我们在访问Http的默认会转发到Https中)
            app.UseHttpsRedirection();
            //  在处理请求管道中使用“MVC架构”(可配置默认路由)
            app.UseMvc();
        }

        /// <summary>
        /// 服务配置
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            //  添加Mvc服务
            services.AddMvc(config =>
            {
                //  输出格式xml
                config.EnableEndpointRouting = false;
                config.ReturnHttpNotAcceptable = true;
                config.OutputFormatters.Add(new XmlSerializerOutputFormatter());
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);   //  设置兼容版本2.2
            //  添加依赖注入服务
            services.AddScoped<IAuthorRepository, AuthorMockRepository>();
            services.AddScoped<IBookRepository, BookMockRepository>();
        }
    }
}