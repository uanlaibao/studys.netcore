﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Library.API.C4
{
    /// <summary>
    /// 应用程序启动类
    /// </summary>
    public class Program
    {
        /// <summary>
        /// 创建一个web主机，并使用启动配置
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();

        public static void Main(string[] args)
        {
            //  创建web主机，生成并运行，运行后将监听http请求
            CreateWebHostBuilder(args).Build().Run();
        }
    }
}
/*
项目Nuget引用：
Microsoft.AspNetCore.JsonPath   3.1.2
Microsoft.VisualStudio.Web.CodeGeneratio    3.1.0

项目目标框架：
.NET Core3.1
 */