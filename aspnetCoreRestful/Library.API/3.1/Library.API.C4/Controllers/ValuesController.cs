﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Library.API.C4.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]//RESTful api设计风格，该设置具有一定约束，但并不强制,RESTfulapi它将url直接连接到一个资源信息，并对资源进行读写操作
    //  RESTful 缺点是不适用于复杂的请求处理，优点是便于对资源数据进行操作。
    //  通常对于超过4个请求参数的url，应使用post请求
    //  示例：https://localhost:44349/api/values
    [ApiController] //  表明这是一个api控制器而不是mvc控制器
    public class ValuesController : ControllerBase  //  控制器基类，继承后将可以读取它的上下文请求信息和进行处理
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
