﻿namespace Library.API.C10.Helpers
{
    public class ApiError
    {
        public string Detail { get; set; }
        public string Message { get; set; }
    }
}