﻿using Library.API.C10.Entities;
using Library.API.C10.Helpers;
using System;
using System.Threading.Tasks;

namespace Library.API.C10.Services
{
    public interface IAuthorRepository : IRepositoryBase<Author>, IRepositoryBase2<Author, Guid>
    {
        Task<PagedList<Author>> GetAllAsync(AuthorResourceParameters parameters);
    }
}