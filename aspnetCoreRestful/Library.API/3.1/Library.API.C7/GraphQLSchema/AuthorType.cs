﻿using GraphQL.Types;
using Library.API.C7.Entities;
using Library.API.C7.Services;

namespace Library.API.C7.GraphQLSchema
{
    public class AuthorType : ObjectGraphType<Author>
    {
        public AuthorType(IRepositoryWrapper repositoryWrapper)
        {
            Field(x => x.Id, type: typeof(IdGraphType));
            Field(x => x.Name);
            Field(x => x.BirthDate);
            Field(x => x.BirthPlace);
            Field(x => x.Email);
            Field<ListGraphType<BookType>>("books", resolve: context =>
            {
                return repositoryWrapper.Book.GetBooksAsync(context.Source.Id).Result;
            });
        }
    }
}