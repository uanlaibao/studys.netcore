﻿using GraphQL.Types;
using Library.API.C7.Entities;

namespace Library.API.C7.GraphQLSchema
{
    public class BookType : ObjectGraphType<Book>
    {
        public BookType()
        {
            Field(x => x.Id, type: typeof(IdGraphType));
            Field(x => x.Title);
            Field(x => x.Description);
            Field(x => x.Pages);
        }
    }
}