﻿using Library.API.C7.Entities;
using Library.API.C7.Helpers;
using System;
using System.Threading.Tasks;

namespace Library.API.C7.Services
{
    public interface IAuthorRepository : IRepositoryBase<Author>, IRepositoryBase2<Author, Guid>
    {
        Task<PagedList<Author>> GetAllAsync(AuthorResourceParameters parameters);
    }
}