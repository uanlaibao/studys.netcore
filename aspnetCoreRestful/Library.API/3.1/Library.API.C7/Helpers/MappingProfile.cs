﻿using AutoMapper;
using Library.API.C7.Entities;
using Library.API.C7.Extensions;
using Library.API.C7.Models;

namespace Library.API.C7.Helpers
{
    public class LibraryMappingProfile : Profile
    {
        public LibraryMappingProfile()
        {
            CreateMap<Author, AuthorDto>()
                .ForMember(dest => dest.Age, config =>
                    config.MapFrom(src => src.BirthDate.GetCurrentAge()));
            CreateMap<AuthorForCreationDto, Author>();
            CreateMap<Book, BookDto>();
            CreateMap<BookForCreationDto, Book>();
            CreateMap<BookForUpdateDto, Book>();
        }
    }
}