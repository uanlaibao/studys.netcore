﻿namespace Library.API.C6.Helpers
{
    public class ApiError
    {
        public string Detail { get; set; }
        public string Message { get; set; }
    }
}