﻿using Library.API.C6.Entities;
using Library.API.C6.Helpers;
using System;
using System.Threading.Tasks;

namespace Library.API.C6.Services
{
    public interface IAuthorRepository : IRepositoryBase<Author>, IRepositoryBase2<Author, Guid>
    {
        Task<PagedList<Author>> GetAllAsync(AuthorResourceParameters parameters);
    }
}