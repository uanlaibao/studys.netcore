﻿using Library.API.C8.Entities;
using Library.API.C8.Helpers;
using System;
using System.Threading.Tasks;

namespace Library.API.C8.Services
{
    public interface IAuthorRepository : IRepositoryBase<Author>, IRepositoryBase2<Author, Guid>
    {
        Task<PagedList<Author>> GetAllAsync(AuthorResourceParameters parameters);
    }
}