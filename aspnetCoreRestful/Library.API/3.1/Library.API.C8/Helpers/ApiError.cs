﻿namespace Library.API.C8.Helpers
{
    public class ApiError
    {
        public string Detail { get; set; }
        public string Message { get; set; }
    }
}