﻿using Library.API.C5.Extentions;
using Microsoft.EntityFrameworkCore;

namespace Library.API.C5.Entities
{
    public class LibraryDbContext : DbContext
    {
        public LibraryDbContext(DbContextOptions<LibraryDbContext> options) : base(options)
        {
        }

        public DbSet<Author> Authors { get; set; }

        public DbSet<Book> Books { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder); //  构建模型
            modelBuilder.SeedData();    //  填充数据
        }
    }
}