﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.API.C5.Entities
{
    public class Book
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Title { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        public int Pages { get; set; }

        [ForeignKey("AuthorId")]    //  ForeignKey指定了外键约束
        public Author Author { get; set; }

        public Guid AuthorId { get; set; }
    }
}
