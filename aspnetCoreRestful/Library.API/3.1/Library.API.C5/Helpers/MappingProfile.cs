﻿using AutoMapper;
using Library.API.C5.Entities;
using Library.API.C5.Extentions;
using Library.API.C5.Models;

namespace Library.API.C5.Helpers
{
    /// <summary>
    /// 这是一个映射类，将源类型转换为目标类型
    /// </summary>
    public class LibraryMappingProfile : Profile
    {
        public LibraryMappingProfile()
        {
            CreateMap<Author, AuthorDto>()
                .ForMember(dest => dest.Age, config =>
                    config.MapFrom(src => src.BirthDate.GetCurrentAge()));
            CreateMap<AuthorForCreationDto, Author>();
            CreateMap<Book, BookDto>();
            CreateMap<BookForCreationDto, Book>();
            CreateMap<BookForUpdateDto, Book>();
        }
    }
}