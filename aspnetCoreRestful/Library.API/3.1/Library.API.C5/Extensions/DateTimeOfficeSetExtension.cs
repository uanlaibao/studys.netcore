﻿using System;

namespace Library.API.C5.Extentions
{
    /// <summary>
    /// 日期偏离
    /// 根据指定日期和当前日期比较获得年龄
    /// </summary>
    public static class DateTimeOffsetExtension
    {
        public static int GetCurrentAge(this DateTimeOffset dateTimeOffset)
        {
            var currentDate = DateTimeOffset.Now;
            int age = currentDate.Year - dateTimeOffset.Year;

            if (currentDate < dateTimeOffset.AddYears(age))
            {
                age--;
            }

            return age;
        }
    }
}