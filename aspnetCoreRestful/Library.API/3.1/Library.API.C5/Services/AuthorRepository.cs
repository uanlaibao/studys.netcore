﻿using Library.API.C5.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace Library.API.C5.Services
{
    public class AuthorRepository : RepositoryBase<Author, Guid>, IAuthorRepository
    {
        public AuthorRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
