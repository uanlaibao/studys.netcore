﻿using Library.API.C5.Entities;
using System;

namespace Library.API.C5.Services
{
    public interface IAuthorRepository : IRepositoryBase<Author>, IRepositoryBase2<Author, Guid>
    {
    }
}
