﻿using Library.API.C5.Entities;

namespace Library.API.C5.Services
{
    /// <summary>
    /// 该存储库用于填充数据上下文对象。
    /// 引用该类型的实例，可以访问数据上下文以及作者、书籍的上下文对象
    /// </summary>
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private IAuthorRepository _authorRepository = null;
        private IBookRepository _bookRepository = null;

        //  自动构造器注入数据上下文对象
        public RepositoryWrapper(LibraryDbContext libraryDbContext)
        {
            LibraryDbContext = libraryDbContext;
        }

        //  将数据上下文对象注入到书籍和作者仓库示例中
        public IAuthorRepository Author => _authorRepository ?? new AuthorRepository(LibraryDbContext);
        public IBookRepository Book => _bookRepository ?? new BookRepository(LibraryDbContext);
        public LibraryDbContext LibraryDbContext { get; }
    }
}