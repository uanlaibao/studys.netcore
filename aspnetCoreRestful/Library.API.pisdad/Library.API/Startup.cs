using Library.API.Entities;
using Library.API.Repositories;
using Library.API.Repositories.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Library.API.Filters;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;

namespace Library.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContextPool<LibraryDbContext>(option =>
            {
                // , optionBuilder => optionBuilder.MigrationsAssembly(typeof(Startup).Assembly.GetName().Name)
                option.UseMySql(Configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddIdentity<User, Role>()
                    .AddEntityFrameworkStores<LibraryDbContext>();

            var csredis = new CSRedis.CSRedisClient(Configuration.GetConnectionString("RedisConnectionString"));
            RedisHelper.Initialization(csredis);
            services.AddSingleton<IDistributedCache>(new Microsoft.Extensions.Caching.Redis.CSRedisCache(RedisHelper.Instance));

            services.AddControllers(configure =>
            {
                configure.CacheProfiles.Add("Default", new CacheProfile { Duration = 60 });
                configure.Filters.Add<JsonExceptionFilter>();
            }).AddNewtonsoftJson();

            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();

            services.AddAutoMapper(typeof(Startup));

            services.AddScoped<CheckAuthorExistFilterAttribute>();
            services.AddScoped<CheckIfMatchHeaderFilterAttribute>();

            // 服务器端缓存
            services.AddResponseCaching(options =>
            {
                options.UseCaseSensitivePaths = true;
                options.MaximumBodySize = 1024;
            });

            // api版本
            services.AddApiVersioning(options =>
            {
                // 指明当客户端未提供版本时是否使用默认版本，默认为false
                options.AssumeDefaultVersionWhenUnspecified = true;

                // 指明了默认版本
                options.DefaultApiVersion = new ApiVersion(1, 0);

                // 指明是否在HTTP响应消息头中包含api-supported-versions和api-deprecated-versions这两项
                options.ReportApiVersions = true;

                // 同时支持多种方式
                // 1.HeaderApiVersionReader：使用自定义 HTTP 消息头访问，在消息头添加 api-version 项
                // 2.QueryStringApiVersionReader：使用查询字符串,参数名为ver,默认为api-version
                options.ApiVersionReader = ApiVersionReader.Combine(
                                                new QueryStringApiVersionReader("ver"),
                                                new HeaderApiVersionReader("api-version"));
            });

            #region 认证和授权
            var tokenSection = Configuration.GetSection("Security:Token");

            // defaultScheme用于指定当未指定具体认证方案时将会使用的默认方案
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuer = true,
                    ValidateIssuerSigningKey = true,

                    // 合法的签发者
                    ValidIssuer = tokenSection[JwtRegisteredClaimNames.Iss],

                    // 合法的接受方
                    ValidAudience = tokenSection[JwtRegisteredClaimNames.Aud],

                    // 签名验证的安全密钥
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenSection["Key"])),

                    // 验证时间的时间偏移值
                    ClockSkew = TimeSpan.Zero
                };
            });
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseResponseCaching();
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
