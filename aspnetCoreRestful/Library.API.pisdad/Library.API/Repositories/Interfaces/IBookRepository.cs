﻿using Library.API.Entities;
using Library.API.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.API.Repositories.Interfaces
{
    public interface IBookRepository : IRepositoryBase<Book>, IRepositoryBase2<Book, Guid>
    {
        Task<IEnumerable<Book>> GetBooksAsync(Guid authorId);
        Task<Book> GetBookAsync(Guid authorId, Guid bookId);
    }
}
