﻿using Library.API.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.Repositories.Interfaces
{
    public interface IRepositoryWrapper
    {
        IBookRepository Book { get; }
        IAuthorRepository Author { get; }
        RoleManager<Role> RoleManager { get; }
        UserManager<User> UserManager { get; }
    }
}
