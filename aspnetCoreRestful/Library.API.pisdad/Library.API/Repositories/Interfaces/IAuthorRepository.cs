﻿using Library.API.Entities;
using Library.API.Helpers;
using Library.API.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.API.Repositories.Interfaces
{
    public interface IAuthorRepository : IRepositoryBase<Author>, IRepositoryBase2<Author, Guid>
    {
        Task<Author> GetAuthorAsync(Guid authorId);
        Task<PagedList<Author>> GetAllAsync(AuthorResourceParameters parameters);
    }
}
