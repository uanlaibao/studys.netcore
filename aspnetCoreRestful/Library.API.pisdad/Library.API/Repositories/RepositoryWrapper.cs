﻿using Library.API.Entities;
using Library.API.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.Repositories
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        public LibraryDbContext LibraryDbContext { get; }

        private IAuthorRepository _authorRepository = null;

        private IBookRepository _bookRepository = null;

        public IBookRepository Book => this._bookRepository ?? new BookRepository(this.LibraryDbContext);

        public IAuthorRepository Author => this._authorRepository ?? new AuthorRepository(this.LibraryDbContext);

        public RoleManager<Role> RoleManager { get; }

        public UserManager<User> UserManager { get; }

        public RepositoryWrapper(LibraryDbContext libraryDbContext, RoleManager<Role> roleManager, UserManager<User> userManager)
        {
            this.LibraryDbContext = libraryDbContext;
            this.RoleManager = roleManager;
            this.UserManager = userManager;
        }
    }
}
