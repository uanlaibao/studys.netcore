﻿using System;

namespace Library.API.Extensions
{
    public static class BirthdayExtension
    {
        public static int GetCurrentAge(this DateTime dto)
        {
            return DateTime.Now.Year - dto.Year;
        }
    }
}
