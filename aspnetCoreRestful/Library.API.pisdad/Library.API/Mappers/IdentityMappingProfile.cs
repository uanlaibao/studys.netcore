﻿using AutoMapper;
using Library.API.Entities;
using Library.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.Mappers
{
    public class IdentityMappingProfile : Profile
    {
        public IdentityMappingProfile()
        {
            this.CreateMap<RegisterUser, User>();
            this.CreateMap<User, UserDto>();
        }
    }
}
