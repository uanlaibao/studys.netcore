﻿using AutoMapper;
using Library.API.Entities;
using Library.API.Extensions;
using Library.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.Mappers
{
    public class LibraryMappingProfile : Profile
    {
        public LibraryMappingProfile()
        {
            CreateMap<Author, AuthorDto>()
                .ForMember(dest => dest.Age, config =>
                    config.MapFrom(src => src.BirthDate.GetCurrentAge()));
            CreateMap<Book, BookDto>();
            CreateMap<AuthorForCreationDto, Author>()
                .ForMember(dest => dest.BirthDate, config => config.MapFrom(src => src.Age));
            CreateMap<AuthorForUpdateDto, Author>()
                .ForMember(dest => dest.BirthDate, config => config.MapFrom(src => src.Age))
                .ReverseMap();
            CreateMap<BookForCreationDto, Book>();
            CreateMap<BookForUpdateDto, Book>()
                .ReverseMap();
        }
    }
}
