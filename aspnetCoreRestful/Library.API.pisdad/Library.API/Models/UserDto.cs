﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.Models
{
    public class UserDto
    {
        public string UserName { get; set; }

        public string Email { get; set; }

        public DateTimeOffset BirthDate { get; set; }

        public string PhoneNumber { get; set; }
    }
}
