﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.API.Models
{
    public class GrantDto
    {
        public string UserId { get; set; }

        public string RoleName { get; set; }
    }
}
