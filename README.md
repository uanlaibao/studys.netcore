# studys.netcore

#### 介绍
基于书籍ASP.NET Core与RESTful api实战做的笔记的源码项目

#### 软件架构
.NET Core3.0

#### 使用说明
VS2019以上

#### 参与贡献

1.  名字什么都是浮云

#### 项目
1.	aspnetCoreRestful	它是《asp.net core与restful api开发实战》这本书的源码示例
2.	CoreShop-master	它是码云上关于asp.net core一个比较现实的开源项目。供参考学习。

#### 更新说明

1.  本期将会更新ASP.NET与RESTful API源码中的功能介绍。说明内容将会记录在源码行中。
2.  下期将会引入简易示例项目。方便学习。
